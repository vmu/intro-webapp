import RequestWatcher from './request-watcher';

// let _headers = {
//   Accept: 'application/json',
//   'Content-Type': 'application/json'
// };

// export function headers() {
//   return _headers;
// }

// URL: https://developer.mozilla.org/en-US/docs/Web/API/URL/URL
// URLSearchParam: https://developer.mozilla.org/en-US/docs/Web/API/URLSearchParams
// Fetch: https://developer.mozilla.org/en-US/docs/Web/API/Fetch_API/Using_Fetch

export async function getToken() {
  return undefined;
}

// API Config
const config = {
  server: {
    api: 'http://localhost:8080',
    static: 'http://localhost:8080'
  }
}

export function getQueryString(params) {
  const searchParams = new URLSearchParams(params);
  return searchParams.toString();
}

export function getApiUri(apiPath) {
  let { path, params } = apiPath;
  if (typeof apiPath === 'string') {
    path = apiPath;
    params = undefined;
  }

  let base = config.server.api;
  if (typeof base === 'string' && typeof path === 'string') {
    if (base.endsWith('/')) {
      base = base.substr(0, base.length - 1);
    }

    let uri;

    if (!path.startsWith('/')) {
      uri = `${base}/${path}`;
    } else {
      uri = base + path;
    }

    if (params) {
      const querystring = getQueryString(params);
      if (querystring) {
        uri = `${uri}?${querystring}`;
      }
    }

    return uri;
  }

  return path;
}

export function parseJSON(response) {
  if (response.ok) {
    return response.json();
  }
  return Promise.reject(response);
}

// export function updateHeaders(newHeaders) {
//   _headers = { ..._headers, ...newHeaders };
//   Object.keys(_headers).forEach((key) => {
//     if (undefined === _headers[key]) {
//       delete _headers[key];
//     }
//   });
// }

export function uploadRequest(apiPath, body, options, contentType) {
  return getToken().then((idToken) => {
    // console.log(`Token: ${idToken}`);
    // Update idToken
    const headers = {
      Accept: 'application/json',
    };

    if (idToken) {
      headers.Authorization = `Bearer ${idToken}`;
    }

    if (contentType && contentType !== 'multipart/form-data') {
      headers['Content-Type'] = contentType;
    }

    const defaultOptions = {
      method: 'POST',
      headers,
    };

    options = { ...defaultOptions, ...options, ...{ body } };

    return fetch(getApiUri(apiPath), options).then(parseJSON);
  });
}

export async function restRequest(apiPath, options) {
  const idToken = await getToken();

  // console.log(`Token: ${idToken}`);
  const headers = {
    Accept: 'application/json',
    'Content-Type': 'application/json',
  };

  // Update idToken
  if (idToken) {
    headers.Authorization = `Bearer ${idToken}`;
  }

  const defaultOptions = {
    method: 'GET',
    headers,
  };

  options = { ...defaultOptions, ...options };

  return fetch(getApiUri(apiPath), options).then(parseJSON);
}

export function getRequest(apiPath, options) {
  return restRequest(apiPath, options);
}

export function postRequest(apiPath, data, options) {
  return restRequest(apiPath, { ...{ method: 'POST', body: JSON.stringify(data) }, ...options });
}

export function putRequest(apiPath, data, options) {
  return restRequest(apiPath, { ...{ method: 'PUT', body: JSON.stringify(data) }, ...options });
}

export function deleteRequest(apiPath, options) {
  return restRequest(apiPath, { ...{ method: 'DELETE' }, ...options });
}

export const requestWatcher = new RequestWatcher();

// Return full static url
export function getStaticUrl(path) {
  if (path.startsWith('//') || path.startsWith('http://') || path.startsWith('https://')) {
    return path;
  }

  let base = config.server.static;
  if (typeof base === 'string' && typeof path === 'string') {
    if (base.endsWith('/')) {
      base = base.substr(0, base.length - 1);
    }

    let uri;

    if (!path.startsWith('/')) {
      uri = `${base}/${path}`;
    } else {
      uri = base + path;
    }

    return uri;
  }

  return path;
}
