import React from "react";
// @material-ui/core components
import { makeStyles } from "@material-ui/core/styles";
import InputLabel from "@material-ui/core/InputLabel";
// core components
import GridItem from "components/Grid/GridItem.js";
import GridContainer from "components/Grid/GridContainer.js";
import CustomInput from "components/CustomInput/CustomInput.js";
import Button from "components/CustomButtons/Button.js";
import Card from "components/Card/Card.js";
import CardHeader from "components/Card/CardHeader.js";
import CardAvatar from "components/Card/CardAvatar.js";
import CardBody from "components/Card/CardBody.js";
import CardFooter from "components/Card/CardFooter.js";

import { withStyles } from '@material-ui/styles';
import { getRequest, postRequest, putRequest, deleteRequest } from '../../api/utils';

import avatar from "assets/img/faces/marc.jpg";

const styles = {
  cardCategoryWhite: {
    color: "rgba(255,255,255,.62)",
    margin: "0",
    fontSize: "14px",
    marginTop: "0",
    marginBottom: "0"
  },
  cardTitleWhite: {
    color: "#FFFFFF",
    marginTop: "0px",
    minHeight: "auto",
    fontWeight: "300",
    fontFamily: "'Roboto', 'Helvetica', 'Arial', sans-serif",
    marginBottom: "3px",
    textDecoration: "none"
  }
};


class NewStudent extends React.Component {
  constructor(props) {
    super(props);
    this.state = { status: 'done' };
    this.handleChange = this.handleChange.bind(this);
    this.handleMsvChange = this.handleMsvChange.bind(this);
    this.handleNameChange = this.handleNameChange.bind(this);
    this.handleCreateStudent = this.handleCreateStudent.bind(this);
  }

  handleMsvChange(event) {
    this.handleChange(event, 'msv');
  }

  handleNameChange(event) {
    this.handleChange(event, 'name');
  }

  handleChange(event, field) {
    this.setState( {[field]: event.target.value});
  }

  handleCreateStudent() {
    console.log(this.state);
    const student = {
      msv: this.state.msv,
      name: this.state.name
    };
    this.setState({ state: 'saving' });
    postRequest(`/student/${this.state.msv}`, student).then(() => {
      this.setState({ state: 'done' });
    });
  }

  render() {
    const { classes } = this.props;
    return (
      <div>
        <GridContainer>
          <GridItem xs={12} sm={12} md={8}>
            <Card>
              <CardHeader color="primary">
                <h4 className={classes.cardTitleWhite}>Create student</h4>
                <p className={classes.cardCategoryWhite}>Fill student infomation</p>
              </CardHeader>
              <CardBody>
                <GridContainer>
                  <GridItem xs={12} sm={12} md={6}>
                    <CustomInput
                      labelText="MSV"
                      id="msv"
                      formControlProps={{
                        fullWidth: true
                      }}
                      inputProps={{
                        onChange: this.handleMsvChange,
                        value: this.state.msv
                      }}
                    />
                  </GridItem>
                  <GridItem xs={12} sm={12} md={6}>
                    <CustomInput
                      labelText="Name"
                      id="name"
                      formControlProps={{
                        fullWidth: true
                      }}
                      inputProps={{
                        onChange: this.handleNameChange,
                        value: this.state.name
                      }}
                    />
                  </GridItem>
                </GridContainer>
              </CardBody>
              <CardFooter>
                {
                  this.state.status === 'done' ?
                    <Button color="primary" onClick={this.handleCreateStudent}>Create student</Button> :
                    <Button color="primary" onClick={this.handleCreateStudent} disabled>Saving</Button>
                }
                
              </CardFooter>
            </Card>
          </GridItem>
        </GridContainer>
      </div>
    );
  }
}

export default withStyles(styles)(NewStudent);
